# Bioctus

Bioctus is a symbolic package written in Python 3 to create musculoskeletal models.
It is inspired by *Biordb* and *OpenSim* and provide converter for these two softwares.

The particularity of Bioctus is that everything can be symbolic. The symbolic backend uses *symengine*, a C++ version of *sympy* which runs significantly faster.

Creating a model is simple and you only need to define the kinematic chain through frames, bodies and joints. 

Rotation and translations are described easily and use optimized dual quaternion operations in backend to produce **efficient** formulae, *i.e.* with the less arithmetic operations possible. It can be used to express the symbolic system jacobian for instance, allowing faster evaluation compared to homogeneous matrices. 

These optimizations are based on Quaternion-Translations, described in 2021's Dantam article:

> Dantam NT. Robust and efficient forward, differential, and inverse kinematics using dual quaternions. *The International Journal of Robotics Research*. 2021;40(10-11):1087-1105. doi:10.1177/0278364920931948).

## Quick Guide
### Requirements
To use Bioctus you only need to have Python 3 installed on your computer with the following packages:
- numpy
- symengine

### Installation
1. Clone this project on your computer via ```git clone```
2. Open a terminal in the newly created *bioctus* folder
3. Install the package using your favorite python  ```python3 setup.py install```

### Create a simple symbolic model
#### Kinematic chain
```
from bioctus import *
import symengine as se
import numpy as np

model = Model("my_model")
ground = model.ground
```
Everything related to the ground (directly or by a chain of frames) will be automatically associated to this model.

We'll create a first **frame** $f$ translated by a variable $t_x$ in the $x$-direction. For that, you need to define a symbolic element $t_x$, a frame $f$, then link it to its parent which is the ground, then describe how this frame is *displaced* from its parent:
```
tx = se.symbols("tx")
f = Frame(
    name="f", 
    parent=ground, 
    displacements=Translation.from_vector(Vector(tx, 0, 0))
    )
```
Displacements correspond to a Rotation or Translation element or of a list of them. They can be described by symbolic or numeric elements. If a list of displacements is provided, then the left-to-right order will be used when applying the displacements in computations.


Now, we will create a **body** $b$ linked to $f$ through a **joint** $j$. A body inherits all properties of a frame, so we could have used a frame element instead. It has the additional properties of inertia, mass and center of mass.
Bodies are useful because we can place physical points on them (markers or path points for muscles). 

The joint should always be defined after the parent element and before the child element. A joint describe displacements between two objects, but these displacements are specific: they are degrees-of-freedoms.

To specify a degree-of-freedom, you should use a **coordinate** (here $rot_z$) which wraps a symbolic element and informations about its possible range, speed, etc.
```
rot_z = Coordinate("rot_z")
j = Joint(
    name="j", 
    parent=f, 
    displacements=Rotation.axis_angle(axis=Vector.Z(), coordinate=rot_z)
)

b = Body(
    name="b",
    parent=j
)
```
Let's express a point placed in the body $b$ in the ground frame:
```
x, y, z = se.symbols("x y z")
end_effector = Point(x, y, z)

end_effector_in_ground = b.express_in(end_effector, ground)
print(end_effector_in_ground)
```
It will print the following:
```
[tx + x - 2*(x*sin((1/2)*rot_z) + y*cos((1/2)*rot_z))*sin((1/2)*rot_z), y + 2*(x*cos((1/2)*rot_z) - y*sin((1/2)*rot_z))*sin((1/2)*rot_z), z]
```
The method ```express_in``` will return a Point object if the element given is a point, or a Vector object if a vector is given. The computations are different between both elements (translations do not affect vectors, since they are located everywhere).

You can also compute the linear and angular jacobians of this kinematic chain,
either using derivatives of coordinates (the **analytic** jacobian) or using screw theory (the **geometric** jacobian), the latest resulting in less arithmetic operations. Both are equals when using Euler angles.
```
J_linear, J_angular = model.compute_geometric_jacobian(
    point=end_effector,
    parent=b,
    coordinates=[rot_z]
)
print(J_linear)
```
The following will be displayed:
```
[-sin((1/2)*rot_z)*(y + 2*(x*cos((1/2)*rot_z) - y*sin((1/2)*rot_z))*sin((1/2)*rot_z))/sin(acos(cos((1/2)*rot_z)))]
[sin((1/2)*rot_z)*(x - 2*(x*sin((1/2)*rot_z) + y*cos((1/2)*rot_z))*sin((1/2)*rot_z))/sin(acos(cos((1/2)*rot_z)))]
[0]
```

Then:
```
J_analytic = model.compute_analytic_jacobian(
    point=end_effector,
    parent=b,
    coordinates=[rot_z]
)
print(J_analytic)
```
This will return:
```
[-(x*sin((1/2)*rot_z) + y*cos((1/2)*rot_z))*cos((1/2)*rot_z) - 2*((1/2)*x*cos((1/2)*rot_z) + (-1/2)*y*sin((1/2)*rot_z))*sin((1/2)*rot_z)]
[2*((-1/2)*x*sin((1/2)*rot_z) + (-1/2)*y*cos((1/2)*rot_z))*sin((1/2)*rot_z) + (x*cos((1/2)*rot_z) - y*sin((1/2)*rot_z))*cos((1/2)*rot_z)]
[0]
```

### Evaluation of symbolic expressions
Still using the previously defined model, you can evaluate the jacobians via the symengine function ```subs()``` as follows:

```
J_linear_evaluated = J_linear.subs({
    "tx": 0.2,
    "rot_z": se.pi / 4, # must be in radian!
    "x": 1,
    "y": 1,
    "z": 0
})
J_linear_evaluated = np.array(J_linear_evaluated).astype(float)
print(J_linear_evaluated)
```
This will print:
```
[[-1.41421356]
 [ 0.        ]
 [ 0.        ]]
```

### Faster evaluation of symbolic expressions
When the kinematic chain is too big, using the ```subs()``` function leads to some computation time.
I suggest to convert the symbolic symengine matrix to a numpy function. This is called the lambdification process and applies as follows:
```
J_linear_fct = se.Lambdify((tx, rot_z.symbol, x, y, z), J_linear, cse=True)

print(J_linear_fct(0.2, np.pi / 4, 1, 1, 0))
```
If the symbolic object has loads and loads of arithmetic operations, the lambdification can take some minutes. However, when done, the evaluation is extremely fast compared to the ```subs``` function.

#### Muscles

**Still under construction**, bu available in example.

You can look for more examples in the subfolder ```bioctus/examples/```.

## Convert a simple OpenSim model
A simple converter exists (still under construction) from opensim models to bioctus model.

You need to have the package ```opensim``` installed (available from conda).

```
from bioctus import *

model_path = "path_to_osim_model.osim"
model = Model.from_opensim(model_path)

for f in model.frames:
    print(f.name)

for m in model.muscles:
    print(
        m.name,
        m.max_isometric_force,
        m.optimal_fiber_length,
        m.tendon_slack_length,
        m.pennation_angle_at_optimal_fiber_length,
    )
```