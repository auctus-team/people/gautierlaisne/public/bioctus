import numpy as np
from .functions import Function
from .motor import Motor, Point, Line, Vector, Symbolic
from .hash import Hashable
import symengine as se

class RotationAxis(Hashable):
    """It should always be assumed that a motion is a normalized rotation or translation axis."""
    __slots__ = "x", "y", "z"
    def __init__(self, x, y, z):
        Hashable.__init__(self)
        self.x, self.y, self.z = x, y, z

    @classmethod
    def X(cls):
        return RotationAxis(1,0,0)
    
    @classmethod
    def Y(cls):
        return RotationAxis(0,1,0)

    @classmethod
    def Z(cls):
        return RotationAxis(0,0,1)
    
    def as_list(self):
        return [self.x, self.y, self.z]
    
    def as_vector(self):
        return Vector(self.x, self.y, self.z)

class TranslationAxis(Hashable):
    """It should always be assumed that a motion is a normalized rotation or translation axis."""
    __slots__ = "x", "y", "z"
    def __init__(self, x, y, z):
        Hashable.__init__(self)
        self.x, self.y, self.z = x, y, z
    
    @classmethod
    def X(cls):
        return TranslationAxis(1,0,0)
    
    @classmethod
    def Y(cls):
        return TranslationAxis(0,1,0)

    @classmethod
    def Z(cls):
        return TranslationAxis(0,0,1)
    
    def as_list(self):
        return [self.x, self.y, self.z]
    
    def as_vector(self):
        return Vector(self.x, self.y, self.z)
    
class Component(Hashable):
    __slots__ = "name", "model", "_hash"
    def __init__(self, name):
        Hashable.__init__(self)
        self.name = name
        self.model = None

class Marker(Component):
    __slots__ = "location", "parent", "marker_type"
    def __init__(self, name, location: list, parent: "Frame", marker_type: str = "anatomical"):
        super().__init__(name)
        self.location = location
        self.parent = parent
        self.marker_type = marker_type

    def express_in(self, Q: dict, other: "Frame"):
        """Express the marker position in the other given frame for a joint configuration Q."""
        return self.parent.express_in(Q, self.location, other)
    
    def express_in_ground(self, Q: dict):
        return self.parent.express_in_ground(Q, self.location)
    
    def __str__(self):
        return self.__repr__()
    
    def __repr__(self):
        return "Marker(name=" + self.name + ", parent=" + self.parent.name + ", point="  + str(self.location) + ", marker_type=" + self.marker_type + ")"

class PathPoint(Component):
    __slots__ = "location", "parent"
    def __init__(self, name: str, location: list, parent: "Frame") -> None:
        super().__init__(name)
        self.location = location
        self.parent = parent
    
    def __str__(self):
        return self.__repr__()
    
    def __repr__(self):
        return "PathPoint(name=" + self.name + ", parent=" + self.parent.name + ", point="  + str(self.location) + ")"

    def express_in(self, Q: dict, other: "Frame"):
        """Express the path location in the other given frame for a joint configuration Q."""
        return self.parent.express_in(Q, self.location, other)
    
    def express_in_ground(self, Q: dict):
        return self.parent.express_in_ground(Q, self.location)
    
class GeneralizedCoordinate(Component):
    __slots__ = "symbol", "default_value", "default_speed_value", "range_values", "clamped", "locked", "independent_coordinate", "coupling_constraint", "enforce_constraint"
    def __init__(self, 
            name: str,
            default_value = 0.0, 
            default_speed_value = 0.0, 
            range_values: list = [-np.inf, np.inf],
            clamped: bool = True,
            locked: bool = False,
            independent_coordinate: "GeneralizedCoordinate" = None,
            coupling_constraint: Function = None
        ):
        """_summary_

        Args:
            name (str): name of the coordinate
            default_speed_value (int, optional): _description_. Defaults to 0.
            range_values (list, optional): _description_. Defaults to [0, 2 * se.pi].
            clamped (bool, optional): whether or not the values of the coordinates should be limited to the range. Defaults to True.
            locked (bool, optional): whether or not the values of the coordinates should be constrained to the current default value. Defaults to False.
        """
        super().__init__(name)
        self.default_value = default_value
        self.default_speed_value = default_speed_value
        self.range_values = range_values
        self.clamped = clamped
        self.locked = locked
        
        # It is possible to say that this coordinate value depends on another coordinate value.
        self.independent_coordinate: GeneralizedCoordinate = independent_coordinate
        self.coupling_constraint: Function = coupling_constraint
        self.enforce_constraint: bool = True # If True, it has consequences in the value() method

    @property
    def joint(self):
        """Retrieve the joint associated to this coordinate."""
        return list(self.model.joints_of_coordinate(self))[0] # there is only one (cf. model.finalize())
    
    @property
    def motion(self):
        """Retrieve the joint associated to this coordinate."""
        return list(self.model.motions_of_coordinate(self))[0] # there is only one (cf. model.finalize())
    
    def is_dependent(self) -> bool:
        """Check if the coordinate is dependent on another one."""
        if self.independent_coordinate is not None:
            return True
        return False
    
    def is_constrained(self):
        return self.clamped or self.locked
    
    def is_locked(self):
        """If the coordinate is locked, its values are the default values."""
        return self.locked
    
    def is_clamped(self):
        """If it is True, then the coordinate value will be forced to be enclosed in its defined range."""
        return self.clamped
    
    def value(self, Q: dict):
        if not self.is_dependent():
            value = Symbolic.convert_to_symbolic(Q[f"{self.name}"])
        else:
            # This coordinate value depends on another one, through a function.
            value = self.coupling_constraint.eval(Q[f"{self.independent_coordinate.name}"])

        if self.is_locked():
            value = self.default_value
        if self.is_clamped():
            if Symbolic.element_is_symbolic(value) is True:
                value = se.Piecewise(
                    (value, se.And(value >= self.range_values[0], value <= self.range_values[1])),
                    (self.range_values[0], value < self.range_values[0]),
                    (self.range_values[1], value > self.range_values[1])
                )
            else:
                if value < self.range_values[0]:
                    value = self.range_values[0]
                elif value > self.range_values[1]:
                    value = self.range_values[1]

        return value

class Motion(Component):
    def __init__(self, coordinate: GeneralizedCoordinate | str, axis: RotationAxis | TranslationAxis, function: callable = None):
        super().__init__("")
        if isinstance(coordinate, str):
            coordinate = GeneralizedCoordinate(coordinate)
        self.coordinate = coordinate
        self.axis = axis
        self.function = function
    
    def transform(self, Q: dict):
        coord = self.coordinate
        value = coord.value(Q)

        if self.function is not None:
            value = self.function.eval(value)

        if type(self.axis) is RotationAxis:
            return Motor.rotation(value, *self.axis.as_list())
        else:
            tx, ty, tz = self.axis.as_list()
            return Motor.translation(value*tx, value*ty, value*tz)
        
class Frame(Component):
    def __init__(self, name: str):
        super().__init__(name)
        
    def express_in(self, Q: dict, X: list | Point | Vector | Line, other: "Frame"):
        """Express the marker position in the other given frame for a joint configuration Q."""
        return self.model.express_in(Q, X, self, other)
    
    def express_in_ground(self, Q: dict, X: list | Point | Vector | Line):
        return self.model.express_in_ground(Q, X, self)

class OffsetFrame(Frame):
    __slots__ = "parent", "orientation", "translation"
    def __init__(self, name: str, parent: Frame = None, orientation: list = [0,0,0], translation: list = [0,0,0]):
        super().__init__(name)
        self.parent = parent
        self.orientation = orientation
        self.translation = translation

    def transform(self, Q):
        R = Motor.rotation_euler("XYZ", self.orientation)
        T = Motor.translation(*self.translation)
        return T*R
    
    def transform_inv(self, Q):
        return self.transform(Q).inv()
    
class Ground(Frame):
    def __init__(self, name: str = "ground"):
        super().__init__(name)
    
class Body(Frame):
    def __init__(self, name: str = None, mass = 0.0, mass_center: list = None, inertia: list = None):
        super().__init__(name)
        self.mass = mass
        self.mass_center = mass_center
        self.inertia = inertia

    @property
    def inertia_tensor(self) -> list[list]:
        """Tensor of inertia written in matrix form about this body mass center, with respect to the xyz axes."""
        Ixx, Iyy, Izz, Ixy, Ixz, Iyz = self.inertia

        I = [[Ixx, -Ixy, -Ixz],
             [-Ixy, Iyy, -Iyz],
             [-Ixz, -Iyz, Izz]]
        
        return I

class Joint(Component):
    __slots__ = "motions", "parent", "child"
    def __init__(self, name: str, motions: list[Motion], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name)
        if not isinstance(motions, list):
            motions = [motions]
        self.motions = motions

        
        if isinstance(parent, Body | Ground):
            parent = OffsetFrame(f"{parent.name}_offset", parent=parent)
        if isinstance(child, Body | Ground):
            child = OffsetFrame(f"{child.name}_offset", parent=child)

        self.parent = parent
        self.child = child

    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion
        coord = motion.coordinate

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        axis = motion.axis.as_vector()
        return self.model.express_in(Q, axis, coord, self.parent)
        # raise NotImplementedError("Not implemented yet.")
        

    @property
    def parents(self):
        """List all parents from the starting body/frame to the last one before the joint."""
        p = self.parent
        list_parents = [p]
        while True:
            if type(p) is OffsetFrame:
                p = p.parent
                list_parents.insert(0,p)
            elif isinstance(p, Frame): # so ground, body, frame
                break
            else:
                raise TypeError(f"A parent in joint {self.name} is not a body, frame or offsetframe! It is a {type(p)}")
        return list_parents
    
    @property
    def children(self):
        """List all children frames from the starting one after the joint to the last body/frame."""
        p = self.child
        list_children = [p]
        while True:
            if type(p) is OffsetFrame:
                p = p.parent
                list_children.append(p)
            elif isinstance(p, Frame): # so ground, body, frame
                break
            else:
                raise TypeError(f"A parent in joint {self.name} is not a body, frame or offsetframe! It is a {type(p)}")
        return list_children
    
    @property
    def coordinates(self):
        return set({ m.coordinate for m in self.motions })
    
class CustomAlignedJoint(Joint):
    def __init__(self, name: str, sequence_rotation: str | None, sequence_translation: str | None, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        """Rotations followed by 3 translations, each of them are axis-aligned.
        If transcribing from OpenSim, be EXTREMELY aware that the OSim CustomJoint uses a space-fixed rotation sequence! Followed by translation.
        This means that the sequence_rotation should be in uppercase.
        """
        if sequence_rotation is None:
            sequence_rotation = ""
        if sequence_translation is None:
            sequence_translation = ""

        if len(coordinates) != len(sequence_rotation)+len(sequence_translation):
            raise ValueError(f"{len(sequence_rotation)+len(sequence_translation)} names should be given for a custom joint!")
        
        if sequence_rotation != "" and not (sequence_rotation.islower() or sequence_rotation.isupper()):
            raise ValueError("Only lowercase rotation sequence or uppercase sequence can be defined.")
        if sequence_translation != "" and not (sequence_translation.islower() or sequence_translation.isupper()):
            raise ValueError("Only lowercase translation sequence are required.")
        
        motions = []
        for k, el in enumerate(sequence_rotation):
            if el == "x" or el == "X":
                motions.append(Motion(coordinates[k], RotationAxis.X()))
            elif el == "y" or el == "Y":
                motions.append(Motion(coordinates[k], RotationAxis.Y()))
            elif el == "z" or el == "Z":
                motions.append(Motion(coordinates[k], RotationAxis.Z()))
            else:
                raise ValueError("The rotation sequence contains characters different from 'x', 'y', 'z' or 'X', 'Y', 'Z'.")
        if sequence_rotation.isupper(): motions.reverse() # extrinsic/intrinsic rotations, meaning that if XYZ, Z should be first in the list since it will be applied first then Y then X
            
        for i, el in enumerate(sequence_translation):
            k = i + len(sequence_rotation) 
            if el == "x" or el == "X":
                motions.append(Motion(coordinates[k], TranslationAxis.X()))
            elif el == "y" or el == "Y":
                motions.append(Motion(coordinates[k], TranslationAxis.Y()))
            elif el == "z" or el == "Z":
                motions.append(Motion(coordinates[k], TranslationAxis.Z()))
            else:
                raise ValueError("The translation sequence contains characters different from 'x', 'y', 'z' or 'X', 'Y', 'Z'.")
        
        super().__init__(name, motions, parent, child)

class SliderJoint(CustomAlignedJoint):
    def __init__(self, name: str, coordinate: str | GeneralizedCoordinate, parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, None, "X", [coordinate], parent, child)
        
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        return motion.axis.as_vector()
    
class PlanarJoint(CustomAlignedJoint):
    def __init__(self, name: str, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, "Z", "XY", coordinates, parent, child)
    
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion
        coord = motion.coordinate

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        axis = motion.axis.as_vector()
        if motion is self.motions[0]: # Rz
            return axis
        elif motion is self.motions[1]: #Tx
            return self.model.express_in(Q, axis, coord, self.parent)
        elif motion is self.motions[2]: # Ty
            return self.model.express_in(Q, axis, coord, self.parent)
    
class PinJoint(CustomAlignedJoint):
    def __init__(self, name: str, coordinate: str | GeneralizedCoordinate, parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, "Z", None, [coordinate], parent, child)
        
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        return motion.axis.as_vector()
    
class UniversalJoint(CustomAlignedJoint):
    def __init__(self, name: str, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, "XY", None, coordinates, parent, child)
        
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion
        
        coord = motion.coordinate

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
            
        axis = motion.axis.as_vector()
        if motion is self.motions[0]:
            return axis
        elif motion is self.motions[1]:
            return self.model.express_in(Q, axis, coord, self.parent)
        
class FreeJoint(CustomAlignedJoint):
    def __init__(self, name: str, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, "XYZ", "XYZ", coordinates, parent, child)
        
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        return motion.axis.as_vector()
        
class EulerJoint(CustomAlignedJoint):
    """A euler joint with 3 rotation sequence."""
    def __init__(self, name: str, sequence: str, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, sequence, None, coordinates, parent, child)

class GimbalJoint(CustomAlignedJoint):
    """A gimbal joint with rotation sequence 'XYZ'"""
    def __init__(self, name: str, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, "XYZ", None, coordinates, parent, child)
        
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion
        coord = motion.coordinate

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        axis = motion.axis.as_vector()
        return self.model.express_in(Q, axis, coord, self.parent)
        
class BallJoint(CustomAlignedJoint):
    """The difference with a gimbal joint with rotation sequence 'XYZ' lies in the description of the angles.
    Here, we use quaternions."""
    def __init__(self, name: str, coordinates: list[str | GeneralizedCoordinate], parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, "XYZ", None, coordinates, parent, child)
        
    def express_angular_velocity_in_parent(self, Q, coordinate_or_motion: GeneralizedCoordinate | Motion):
        motion = coordinate_or_motion
        if isinstance(coordinate_or_motion, GeneralizedCoordinate):
            motion = coordinate_or_motion.motion

        if motion not in self.motions:
            raise ValueError("The given motion or coordinate is not linked to this joint!")
        
        return motion.axis.as_vector()
    
class WeldJoint(Joint):
    def __init__(self, name: str, parent: Body | OffsetFrame, child: Body | OffsetFrame):
        super().__init__(name, [], parent, child)
        
class Actuator(Component):
    def __init__(self, name: str, path_points: list[PathPoint]):
        super().__init__(name)
        self.path_points = path_points

    def path_point(self, name: str):
        for x in self.path_points:
            if x.name == name:
                return x
        raise ValueError("There is no point with this name in this actuator.")
    
    def geometric_length(self, Q: dict):
        """Corresponds to the geometric length, from via point to via point considered as segments."""
        length = 0

        for i in range(len(self.path_points) - 1):
            common_frame = self.path_points[0].parent

            p1 = self.path_points[i]
            p2 = self.path_points[i + 1]

            p1_ = Point(p1.parent.express_in(Q, p1.location, common_frame))
            p2_ = Point(p2.parent.express_in(Q, p2.location, common_frame))
            
            length += (p2_ - p1_).norm()
        return length

    def path_points_around_coordinate(self, coordinate: GeneralizedCoordinate):
        """Retrieve subsets of 2 path points which are around the joint of the coordinate."""
        joint = coordinate.joint.parent
        points_around_joint = []

        for k in range(len(self.path_points) - 1):
            p1 = self.path_points[k]
            p2 = self.path_points[k + 1]

            source = p1.parent
            target = p2.parent
            try:
                path = self.model.shortest_path(source, target)
                if joint in path:
                    points_around_joint.append((p1, p2))
            except:
                try:
                    path = self.model.shortest_path(target, source)
                    if joint in path:
                        points_around_joint.append((p2, p1))
                except: # No found path
                    pass
        
        return points_around_joint
        
    def lever_arm(self, Q: dict, coordinates: str | list | GeneralizedCoordinate = []):
        """Returns the signed orthogonal distance from the closest sub-path to the given coordinate"""
        if isinstance(coordinates, str):
            coordinates = [self.model.coordinate(coordinates)]
        elif isinstance(coordinates, list):
            new_coords = []
            for c in coordinates:
                if isinstance(c, GeneralizedCoordinate): new_coords.append(c)
                elif isinstance(c, str): new_coords.append(self.model.coordinate(c))
            coordinates = new_coords
        elif isinstance(coordinates, GeneralizedCoordinate):
            coordinates = [coordinates]

        lever_arm_vector = []
        for c in coordinates:
            joint = c.joint
            motion = c.motion

            w = joint.express_angular_velocity_in_parent(Q, motion)

            path_points = self.path_points_around_coordinate(c)
            
            if len(path_points) == 0:
                lever_arm_vector.append(0)
            elif len(path_points) == 1:
                p1, p2 = path_points[0]

                if isinstance(motion.axis, RotationAxis):
                    p1_at_end_of_joint = p1.express_in(Q, joint.child) # they are list
                    p2_at_end_of_joint = p2.express_in(Q, joint.child)

                    p1_ = Vector(p1_at_end_of_joint)
                    p2_ = Vector(p2_at_end_of_joint)

                    p1_in_joint = joint.child.express_in(Q, p1_, joint.parent).as_point()
                    p2_in_joint = joint.child.express_in(Q, p2_, joint.parent).as_point()
                else:
                    p1_in_joint = p1.express_in(Q, joint.parent)
                    p2_in_joint = p2.express_in(Q, joint.parent)
                L = Line.from_points(p1_in_joint, p2_in_joint)
                
                # NEVER FORGET: The translation-world is like a mirror of the rotation-world.
                # Lever arm in our 3D world is associated to the direction of line in the mirror world
                if isinstance(motion.axis, RotationAxis):
                    lever_arm_vector.append(w.dot(L.lever_arm()))
                elif isinstance(motion.axis, TranslationAxis):
                    lever_arm_vector.append(w.dot(L.direction().normal()))
            else:
                raise Exception("Only actuator with only one set of 2 path points can be considered!")

        if len(lever_arm_vector) == 1:
            return lever_arm_vector[0]
        return lever_arm_vector
            
class Muscle(Actuator):
    def __init__(self, name: str, path_points: list[PathPoint]):
        super().__init__(name, path_points)

class HillThelenMuscle(Muscle):
    def __init__(self, name: str, path_points: list[PathPoint], 
        max_isometric_force=1000,
        optimal_fiber_length=0.2,
        tendon_slack_length=0.1,
        pennation_angle_at_optimal_fiber_length=0.0, 
    ):
        super().__init__(name, path_points)
        self.max_isometric_force = max_isometric_force
        self.optimal_fiber_length = optimal_fiber_length
        self.tendon_slack_length = tendon_slack_length
        self.pennation_angle_at_optimal_fiber_length = (
            pennation_angle_at_optimal_fiber_length
        )

    def musculotendon_length(self):
        """Corresponds to the geometric length, from via point to via point considered as segments."""
        return self.geometric_length()

    def normalized_musculotendon_length(self):
        """The geometric length normalized by the optimal fiber length"""
        return self.musculotendon_length() / self.optimal_fiber_length

    def muscle_length(self):
        """The muscle length corresponds to the geometric length without the tendon part."""
        length = (self.musculotendon_length() - self.tendon_slack_length) / se.cos(
            self.pennation_angle_at_optimal_fiber_length
        )
        return length

    def normalized_muscle_length(self):
        """The muscle length normalized by the optimal fiber length"""
        return self.muscle_length() / self.optimal_fiber_length
    
    def normalized_active_muscle_force(self):
        # https://github.com/pyomeca/biorbd/blob/master/src/InternalForces/Muscles/HillThelenType.cpp
        return se.exp(-((self.normalized_muscle_length() - 1) ** 2) / 0.45)

    def active_muscle_force(self):
        return self.normalized_active_muscle_force() * self.max_isometric_force

    def normalized_passive_muscle_force(self):
        # https://github.com/pyomeca/biorbd/blob/master/src/InternalForces/Muscles/HillThelenType.cpp
        # normalized_muscle_length = self.getLength() / muscle.optimalFiberLength

        kpe = 5.0
        e0 = 0.6
        t5 = se.exp(kpe * (self.normalized_muscle_length() - 1) / e0)
        t7 = se.exp(kpe)

        normalized_passive_muscle_force = (t5 - 1) / (t7 - 1)

        normalized_passive_muscle_force_ = se.Piecewise(
            (
                normalized_passive_muscle_force,
                self.normalized_muscle_length() > 1,
            ),
            (0, True),  # True means otherwise
        )
        return normalized_passive_muscle_force_

    def passive_muscle_force(self):
        return self.normalized_passive_muscle_force() * self.max_isometric_force

    def get_force_from_activation(self, a):
        """'a' can be a symbol"""

        fa = self.normalized_active_muscle_force()
        fp = self.normalized_passive_muscle_force()

        forceFromAct = se.Piecewise(
            (
                self.max_isometric_force
                * (a * fa + fp)
                * se.cos(self.pennation_angle_at_optimal_fiber_length),
                a > 0 + 0.002,
            ),
            (0, True),
        )
        # forceFromAct = self.max_isometric_force * (a * fa + fp) * se.cos(self.pennation_angle_at_optimal_fiber_length)
        return forceFromAct
