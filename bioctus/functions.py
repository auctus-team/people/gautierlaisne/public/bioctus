import symengine as se
import numpy as np
from .motor import Symbolic
from .hash import Hashable

def binary_search(el, X: list):
    """Return the index k where el is the closest on the right. 
    i.e. X[k] < el"""
    n = len(X)
    i = 0
    j = n
    while True:
        k = (i+j)/2
        k = int(k)
        if el < X[k]:
            j = k
        elif el > X[k+1]:
            i = k
        else:
            break
    return k

TINY_NUMBER = 0.000001
ROUNDOFF_ERROR = 0.0000000000002

class Function(Hashable, Symbolic):
    __slots__ = "name"
    def __init__(self, name: str):
        Symbolic.__init__(self)
        Hashable.__init__(self)
        self.name = name

    def eval(self, x):
        raise NotImplementedError("This function has no evaluation function defined.")
    
    def derivative(self, x):
        raise NotImplementedError("This function has no derivative function defined.")
    
    def to_list(self):
        raise NotImplementedError("This function has no list defined.")
    
class Identity(Function):
    def __init__(self, name: str = "identity"):
        super().__init__(name)
    
    def eval(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return x
    
    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return 0
    
    def to_list(self):
        return []
    
    
class Constant(Function):
    def __init__(self, name: str, constant):
        super().__init__(name)
        self.constant = Symbolic.convert_to_symbolic(constant)
    
    def eval(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return self.constant
    
    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return 0
    
    def to_list(self):
        return [self.constant]
    
class Linear(Function):
    def __init__(self, name: str, slope, intercept):
        super().__init__(name)
        self.slope, self.intercept = Symbolic.convert_to_symbolic([slope, intercept])
    
    def eval(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return self.slope * x + self.intercept
    
    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return self.slope

    def to_list(self):
        return [self.slope, self.intercept]
    
class Polynomial(Function):
    def __init__(self, name: str, coefficients: list):
        """The first n-1 coefficients are from the highest to lowest. The last
        element of the list should the intercept."""
        super().__init__(name)
        self.coefficients = Symbolic.convert_to_symbolic(coefficients)

    def eval(self, x):  
        x = Symbolic.convert_to_symbolic(x)
        res = 0
        for k in range(len(self.coefficients)):
            n = len(self.coefficients) - k - 1
            a = self.coefficients[n]
            res += a*(x**n)
            
        return res
    
    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        res = 0
        for k in range(len(self.coefficients)):
            n = len(self.coefficients) - k - 1
            a = self.coefficients[n]

            if n == 0:
                a = 0
            res += a*(x**(n-1))
            
        return res
    
    def to_list(self):
        return self.coefficients

class Multiplier(Function):
    def __init__(self, name: str, scale_factor = 1.0, sub_fct: Function = Identity()):
        super().__init__(name)
        self.scale_factor = Symbolic.convert_to_symbolic(scale_factor)
        self.sub_fct = sub_fct
    
    def eval(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return self.scale_factor * self.sub_fct.eval(x)
    
    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return self.scale_factor * self.sub_fct.derivative(x)
    
    def to_list(self):
        return [self.scale_factor] + self.sub_fct.to_list()

class PiecewiseConstant(Function):
    def __init__(self, name: str, X: list, Y: list):
        """For each interval [X[i], X[i+1]], returns Y[i].
        The X list should have the same number of elements as the Y list"""
        super().__init__(name)
        if len(X) != len(Y):
            raise ValueError("The two list should have the same number of elements.")
        
        self.X = Symbolic.convert_to_symbolic(X)
        self.Y = Symbolic.convert_to_symbolic(Y)
    
    def to_list(self):
        return self.X + self.Y
    
    def eval(self, x):
        """Before evaluating, if you do it ensure you are using exactly the same type as the elements in the X and Y matrices!!!!
        Otherwise, expect a kernel crash."""
        x = Symbolic.convert_to_symbolic(x)
        X = self.X
        Y = self.Y
        n = len(X)

        symbolic = self.is_symbolic() or isinstance(x, se.Expr)
        if symbolic:
            rd = np.random.randint(low=1000,high=10000000)
            x_se = se.symbols(f"x_var_{rd}")

            to_piecewise = [
                (Y[0], se.Or(x_se < X[0], se.Abs(x_se - X[0]) <= ROUNDOFF_ERROR)),
                (Y[n-1], se.Or(x_se >= X[n-1], se.Abs(x_se - X[n-1]) <= ROUNDOFF_ERROR))
            ]
            
            for k in range(len(X)-1):
                to_piecewise.append(
                    (Y[k], se.And(x_se >= X[k], x_se < X[k+1]))
                )
            to_piecewise.append((Y[n-1], se.Or(x_se >= X[n-1], se.Abs(x_se - X[n-1]) <= ROUNDOFF_ERROR)))
            
            return se.Piecewise(*to_piecewise).subs({x_se: x})
        else:
            if x < X[0] or np.abs(x - X[0]) <= 0:
                return Y[0]
            elif x >= X[n-1] or np.abs(x - X[n-1]) <= 0:
                return Y[n-1]
            else:
                for k in range(len(X) - 1):
                    if x >= X[k] and x < X[k+1]:
                        return Y[k] 
    
    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        return 0


class PiecewiseLinear(Function):
    def __init__(self, name: str, X: list, Y: list):
        super().__init__(name)    
        self.X = Symbolic.convert_to_symbolic(X)
        self.Y = Symbolic.convert_to_symbolic(Y)

        self.slopes = []
        for k in range(len(X)-1):
            self.slopes.append((self.Y[k+1] - self.Y[k]) / (self.X[k+1] - self.X[k]))
        self.slopes.append(self.slopes[-1])
    
    def to_list(self):
        return self.X + self.Y
    
    def eval(self, x):
        """Before evaluating, if you do it ensure you are using exactly the same type as the elements in the X and Y matrices!!!!
        Otherwise, expect a kernel crash."""
        x = Symbolic.convert_to_symbolic(x)
        n = len(self.X)
        X = self.X
        Y = self.Y
        slopes = self.slopes

        symbolic = self.is_symbolic() or isinstance(x, se.Expr)
        if symbolic:
            rd = np.random.randint(low=1000,high=10000000)
            x_se = se.symbols(f"x_var_{rd}")

            to_piecewise = [
                (Y[0] + (x_se - X[0])*slopes[0], x_se < X[0]),
                (Y[0], se.Abs(x_se - X[0]) <= ROUNDOFF_ERROR),
                (Y[n-1], se.Abs(x_se - X[n-1]) <= ROUNDOFF_ERROR),
                (Y[n-1] + (x_se - X[n-1])*slopes[n-1], x_se > X[n-1])
            ]
            
            for k in range(len(X)-1):
                to_piecewise.append(
                    (Y[k] + (x_se - X[k]) * slopes[k], se.And(x_se >= X[k], x_se < X[k+1]))
                )

            return se.Piecewise(*to_piecewise).subs({x_se: x})
        else:
            if x < X[0]:
                return Y[0] + (x - X[0])*slopes[0]
            elif np.abs(x - X[0]) <= 0:
                return Y[0]
            elif np.abs(x - X[n-1]) <= 0:
                return Y[n-1]
            elif x > X[n-1]:
                return Y[n-1] + (x - X[n-1])*slopes[n-1]
            else:
                for k in range(len(X) - 1):
                    if x >= X[k] and x < X[k+1]:
                        return Y[k] + (x - X[k]) * slopes[k]


    def derivative(self, x):
        x = Symbolic.convert_to_symbolic(x)
        n = len(self.X)
        X = self.X
        Y = self.Y
        slopes = self.slopes

        symbolic = self.is_symbolic() or isinstance(x, se.Expr)
        if symbolic:
            rd = np.random.randint(low=1000,high=10000000)
            x_se = se.symbols(f"x_var_{rd}")

            to_piecewise = [
                (slopes[0], x_se < X[0]),
                (slopes[0], se.Abs(x_se - X[0]) <= ROUNDOFF_ERROR),
                (slopes[n-1], se.Abs(x_se - X[n-1]) <= ROUNDOFF_ERROR),
                (slopes[n-1], x_se > X[n-1])
            ]
            
            for k in range(len(X)-1):
                to_piecewise.append(
                    (slopes[k], se.And(x_se >= X[k], x_se < X[k+1]))
                )

            return se.Piecewise(*to_piecewise).subs({x_se: x})
        else:            
            if x < X[0]: return slopes[0]
            elif np.abs(x - X[0]) <= 0: return slopes[0]
            elif np.abs(x - X[n-1]) <= 0: return slopes[n-1]
            elif x > X[n-1]: return slopes[n-1]
            else:
                for k in range(len(X) - 1):
                    if x >= X[k] and x < X[k+1]:
                        return slopes[k]
                    

class SimmSpline(Function):
    def __init__(self, name: str, X: list, Y: list):
        """cf. https://github.com/opensim-org/opensim-core/blob/main/OpenSim/Common/SimmSpline.cpp"""
        super().__init__(name)
        
        self.X = Symbolic.convert_to_symbolic(X)
        self.Y = Symbolic.convert_to_symbolic(Y)
        self.b_symbolic = [0 for _ in range(len(X))]
        self.c_symbolic = [0 for _ in range(len(X))]
        self.d_symbolic = [0 for _ in range(len(X))]
        self.b = [0 for _ in range(len(X))]
        self.c = [0 for _ in range(len(X))]
        self.d = [0 for _ in range(len(X))]

        self.init_coefficients_symbolic()
        self.init_coefficients_numpy()
    
    def to_list(self):
        return self.X + self.Y + self.b_symbolic + self.c_symbolic + self.d_symbolic
    
    def eval(self, x):
        """Before evaluating, if you do it ensure you are using exactly the same type as the elements in the X and Y matrices!!!!
        Otherwise, expect a kernel crash."""
        x = Symbolic.convert_to_symbolic(x)
        symbolic = self.is_symbolic() or isinstance(x, se.Expr)
        if symbolic:
            b, c, d = self.b_symbolic, self.c_symbolic, self.d_symbolic
            n = len(self.X)
            X = self.X
            Y = self.Y

            rd = np.random.randint(low=1000,high=10000000)
            x_se = se.symbols(f"x_var_{rd}")

            to_piecewise = [
                (Y[0], se.Abs(x_se - X[0]) <= ROUNDOFF_ERROR),
                (Y[n-1], se.Abs(x_se - X[n-1]) <= ROUNDOFF_ERROR),
                (Y[0] + (x_se - X[0])*b[0], x_se < X[0]),
                (Y[n-1] + (x_se - X[n-1])*b[n-1], x_se > X[n-1]),
            ]
            
            if n < 3:
                k = 0
                dx = x_se - X[k]
                val = Y[k] + dx*(b[k] + dx*(c[k] + dx*d[k]))
                to_piecewise.append(
                    (val, n < 3)
                )
            else:
                for k in range(len(X)-1):
                    dx = x_se - X[k]
                    to_piecewise.append(
                        (Y[k] + dx*(b[k] + dx*(c[k] + dx*d[k])), se.And(x_se >= X[k], x_se < X[k+1]))
                    )

            # Adding a roundoff_error is NECESSARY to avoid kernel crashes when evaluating at specific X[k] values in float
            return se.Piecewise(*to_piecewise).subs({x_se: x})
        else:
            b, c, d = self.b, self.c, self.d
            n = len(self.X)
            X = self.X
            Y = self.Y
            
            if abs(x - X[0]) <= 0:
                return Y[0]
            elif abs(x - X[n-1]) <= 0:
                return Y[n-1]
            elif x < X[0]: 
                return Y[0] + (x - X[0])*b[0]
            elif x > X[n-1]: 
                return Y[n-1] + (x - X[n-1])*b[n-1]
            else:
                if n < 3:
                    k = 0
                    dx = x - X[k]
                    val = Y[k] + dx*(b[k] + dx*(c[k] + dx*d[k]))
                    return val
                else:
                    for k in range(len(X)-1):
                        dx = x - X[k]
                        if x >= X[k] and x < X[k+1]:
                            return Y[k] + dx*(b[k] + dx*(c[k] + dx*d[k]))

    # def derivative(self, x):
    #     pass

    def init_coefficients_symbolic(self):
        n = len(self.X)
        
        b, c, d = self.b_symbolic, self.c_symbolic, self.d_symbolic

        if n < 2:
            raise ValueError("n inf 2")
        
        if n == 2:
            t = se.Max(TINY_NUMBER, self.X[1] - self.X[0])
            b[0] = (self.Y[1] - self.Y[0]) / t
            b[1] = b[0]
            c[0] = 0
            c[1] = 0
            d[0] = 0
            d[1] = 0
        else: # 3 and more
            nm1 = n - 1
            nm2 = n - 2

            d[0] = se.Max(TINY_NUMBER, self.X[1] - self.X[0])
            c[1] = (self.Y[1] - self.Y[0]) / d[0]

            for i in range(1, nm1):
                d[i] = se.Max(TINY_NUMBER, self.X[i+1] - self.X[i])
                b[i] = 2.0 * (d[i-1] + d[i])
                c[i+1] = (self.Y[i+1] - self.Y[i]) / d[i]
                c[i] = c[i+1] - c[i]

            b[0] = -d[0]
            b[nm1] = -d[nm2]
            c[0] = 0
            c[nm1] = 0
            
            # End conditions. Third derivatives at x[0] and x[n-1] are obtained from divided differences.

            if n > 3:
                d1, d2, d3, d20, d30, d31 = 0, 0, 0, 0, 0, 0
                d20 = se.Max(TINY_NUMBER, self.X[2] - self.X[0])
                d30 = se.Max(TINY_NUMBER, self.X[3] - self.X[0])
                d31 = se.Max(TINY_NUMBER, self.X[3] - self.X[1])
                d1 = se.Max(TINY_NUMBER, self.X[nm1] - self.X[n-3])
                d2 = se.Max(TINY_NUMBER, self.X[nm2] - self.X[n-4])
                d3 = se.Max(TINY_NUMBER, self.X[nm1] - self.X[n-4])

                c[0] = c[2] / d31 - c[1]/d20
                c[nm1] = c[nm2]/d1 - c[n-3]/d2
                c[0] = c[0] * d[0] * d[0] / d30
                c[nm1] = -c[nm1] * d[nm2] * d[nm2] / d3

            # Forward elimination
            for i in range(n):
                t = d[i-1] / b[i-1]
                b[i] -= t * d[i-1]
                c[i] -= t * c[i-1]
            
            # Back substitution
            c[nm1] /= b[nm1]
            for j in range(nm1):
                i = nm2 - j
                c[i] = (c[i] - d[i]*c[i+1]) / b[i]
            
            # Compute polynomial coefficients
            b[nm1] = (self.Y[nm1] - self.Y[nm2]) / d[nm2] + d[nm2]*(c[nm2] + 2.0*c[nm1])
            for i in range(nm1):
                b[i] = (self.Y[i+1] - self.Y[i]) / d[i] - d[i]*(c[i+1]+2.0*c[i])
                d[i] = (c[i+1] - c[i]) / d[i]
                c[i] *= 3.0
            
            c[nm1] *= 3.0
            d[nm1] = d[nm2]

        self.b_symbolic = b
        self.c_symbolic = c
        self.d_symbolic = d

    def init_coefficients_numpy(self):
        n = len(self.X)
        
        if n < 2:
            raise ValueError("n inf 2")
        
        if n == 2:
            t = max(0, self.X[1] - self.X[0])
            self.b[0] = (self.Y[1] - self.Y[0]) / t
            self.b[1] = self.b[0]
            self.c[0] = 0
            self.c[1] = 0
            self.d[0] = 0
            self.d[1] = 0
        else: # 3 and more
            nm1 = n - 1
            nm2 = n - 2
            
            self.d[0] = max(0, self.X[1] - self.X[0])
            self.c[1] = (self.Y[1] - self.Y[0]) / self.d[0]

            for i in range(1, nm1):
                self.d[i] = max(0, self.X[i+1] - self.X[i])
                self.b[i] = 2.0 * (self.d[i-1] + self.d[i])
                self.c[i+1] = (self.Y[i+1] - self.Y[i]) / self.d[i]
                self.c[i] = self.c[i+1] - self.c[i]

            self.b[0] = -self.d[0]
            self.b[nm1] = - self.d[nm2]
            self.c[0] = 0
            self.c[nm1] = 0
            
            # End conditions. Third derivatives at x[0] and x[n-1] are obtained from divided differences.

            if n > 3:
                d1, d2, d3, d20, d30, d31 = 0, 0, 0, 0, 0, 0
                d20 = max(0, self.X[2] - self.X[0])
                d30 = max(0, self.X[3] - self.X[0])
                d31 = max(0, self.X[3] - self.X[1])
                d1 = max(0, self.X[nm1] - self.X[n-3])
                d2 = max(0, self.X[nm2] - self.X[n-4])
                d3 = max(0, self.X[nm1] - self.X[n-4])

                self.c[0] = self.c[2] / d31 - self.c[1]/d20
                self.c[nm1] = self.c[nm2]/d1 - self.c[n-3]/d2
                self.c[0] = self.c[0] * self.d[0] * self.d[0] / d30
                self.c[nm1] = - self.c[nm1] * self.d[nm2] * self.d[nm2] / d3

            # Forward elimination
            for i in range(n):
                t = self.d[i-1] / self.b[i-1]
                self.b[i] -= t * self.d[i-1]
                self.c[i] -= t * self.c[i-1]
            
            # Back substitution
            self.c[nm1] /= self.b[nm1]
            for j in range(nm1):
                i = nm2 - j
                self.c[i] = (self.c[i] - self.d[i]*self.c[i+1]) / self.b[i]
            
            # Compute polynomial coefficients
            self.b[nm1] = (self.Y[nm1] - self.Y[nm2]) / self.d[nm2] + self.d[nm2]*(self.c[nm2] + 2.0*self.c[nm1])
            for i in range(nm1):
                self.b[i] = (self.Y[i+1] - self.Y[i]) / self.d[i] - self.d[i]*(self.c[i+1]+2.0*self.c[i])
                self.d[i] = (self.c[i+1] - self.c[i]) / self.d[i]
                self.c[i] *= 3.0
            
            self.c[nm1] *= 3.0
            self.d[nm1] = self.d[nm2]