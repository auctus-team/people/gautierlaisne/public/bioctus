import uuid

class Hashable:
    __slots__ = "_hash"
    def __init__(self):
        self._hash = uuid.uuid4()

    def __hash__(self):
        return hash(str(self._hash))
