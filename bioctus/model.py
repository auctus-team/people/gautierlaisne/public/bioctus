from .components import FreeJoint, PlanarJoint, PathPoint, HillThelenMuscle, Muscle, Actuator, Marker, Ground, Body, Frame, OffsetFrame, GeneralizedCoordinate, Joint, Motion, RotationAxis, TranslationAxis 
from .motor import Motor, Point, Vector, Line, Symbolic, Matrix
import networkx as nx
import numpy as np
import symengine as se

class Model:
    __slots__ = "name", "ground", "graph", "components", "gravity_constant", "mode"
    def __init__(self, name: str):
        """To create a model, define your bodies, generalized coordinates then the joints.
        For each joint, now use model.link(the_joint). At the end, use model.finalize() to ensure 
        everything's ok.
        
        You should always use the ground of the model."""
        self.name = name
        self.ground = Ground()
        self.graph = nx.DiGraph()
        self.components = set()
        self.gravity_constant = 9.80665
        self.mode: str = None 
        
    def add(self, el):
        el.model = self
        self.components.add(el)

    @staticmethod
    def from_opensim(model):
        """The model should be a string if it is a path to an opensim model, or directly an OpenSim Model object."""
        from .opensim_converter import opensim_to_bioctus # this avoids circular import
        return opensim_to_bioctus(model)
    
    @property
    def gravity(self):
        return Vector([0, -self.gravity_constant, 0])
    
    @property
    def mass(self):
        """Total mass of the model (sum of all the body mass)"""
        return sum([x.mass for x in self.bodies])
    
    @property
    def center_of_mass(self):
        """Corresponds to the barycenter of all bodies center of mass ponderated by their respective mass."""
        X, Y, Z = 0,0,0
        total_mass = self.mass

        for b in self.bodies:
            x,y,z = b.mass_center
            m = b.mass
            X += m * x
            Y += m * y
            Z += m * z
        return [X/total_mass, Y/total_mass, Z/total_mass]

    def link(self, joints: any):
        """Link joints between them"""
        if not isinstance(joints, list):
            joints = [joints]

        for j in joints:
            self.add(j)

            parents = j.parents
            children = j.children
            motions = j.motions
            motions.reverse()
            
            for x in parents:
                self.add(x)
            for x in children:
                self.add(x)
            for x in motions:
                self.add(x)
            for x in j.coordinates:
                self.add(x)
                
            # Adding ordered parents list to the graph 
            for i in range(len(parents) - 1):
                x1 = parents[i]
                x2 = parents[i+1]
                self.graph.add_edge(x1, x2, transform=x2.transform)

            # Adding motions
            for i in range(len(motions) - 1):
                x1 = motions[i]
                x2 = motions[i+1]
                self.graph.add_edge(x1, x2, transform=x2.transform)
            
            # Ordered children list to the graph
            for i in range(len(children) - 1):
                x1 = children[i]
                x2 = children[i+1]
                self.graph.add_edge(x1, x2, transform=x1.transform_inv)

            #  Linking to parents and children lists
            if len(motions) == 0:
                self.graph.add_edge(parents[-1], children[0], transform=None)
            else:
                # Adding the Joint after motions is required to discriminate during the express function
                # This is to ensure the offsetframe after the joint is not necessarily applied if we want to express in it.
                self.graph.add_edge(parents[-1], motions[0], transform=motions[0].transform)
                self.graph.add_edge(motions[-1], children[0], transform=None)
                
    def motions_of_coordinate(self, coord: GeneralizedCoordinate):
        """Return the set of motions in which this coordinate act through a motion."""
        all_motions = set()
        for j in self.joints:
            for m in j.motions:
                if coord == m.coordinate:
                    all_motions.add(m)
        return all_motions
    
    def joints_of_coordinate(self, coord: GeneralizedCoordinate):
        """Return the set of joints in which this coordinate act through a motion."""
        return set({ j for j in self.joints if coord in j.coordinates })
    
    def shortest_path(self, source: any, target: any):
        if isinstance(source, GeneralizedCoordinate):
            motions = self.motions_of_coordinate(source)
            if len(motions) != 1:
                raise ValueError(f"There are {len(motions)} motions associated to this coordinate! Cannot procede.")
            source = list(motions)[0]

        if isinstance(target, GeneralizedCoordinate):
            motions = self.motions_of_coordinate(target)
            if len(motions) != 1:
                raise ValueError(f"There are {len(motions)} motions associated to this coordinate! Cannot procede.")
            target = list(motions)[0]
        
        return nx.shortest_path(self.graph, source, target)
    
    def express_in_ground(self, Q: dict, X: Point | Vector | Line, origin: any):
        return self.express_in(Q, X, origin, self.ground)
    
    def express_in(self, Q: dict, X: list | Point | Vector | Line, origin: any, to_express_in: any):
        initial_type = type(X)
        if type(X) is list:
            X = Point(*X)

        source = to_express_in
        target = origin
        if source == target:
            return X
        
        # Special case: if a joint is given, then when going into this joint means we place the object in the parent frame
        # meaning that the joint motions occur right after the parent frame.
        if isinstance(source, Joint):
            source = source.parent
        if isinstance(target, Joint):
            target = target.parent
        
        path = []
        to_invert = False
        try:
            # source is 'after' the target => should not inverse the final rotor
            path = self.shortest_path(source, target)
        except:
            try:
                # source is 'before' the target => should inverse the final rotor
                path = self.shortest_path(target, source)
                to_invert = True
            except:
                # This should never occur if finalize() has been valid.
                raise Exception("No path between the source and target!")
        
        R = Motor.identity()
        for i in range(len(path) - 1):
            u1 = path[i]
            u2 = path[i+1]
            transform = self.graph.get_edge_data(u1, u2)["transform"]
            if transform is not None:
                R = R*transform(Q)
            
        if to_invert:
            R = R.inv()
        
        result = R.apply(X)
        if initial_type is list:
            return result.to_list()
        return result
    
    @property
    def frames(self):
        return set({ x for x in self.components if isinstance(x, Frame)})
    
    @property
    def offset_frames(self):
        return set({ x for x in self.components if isinstance(x, OffsetFrame)})

    @property
    def bodies(self):
        return set({ x for x in self.components if isinstance(x, Body)})

    @property
    def joints(self):
        return set({ x for x in self.components if isinstance(x, Joint)})
    
    @property
    def coordinates(self):
        return set({ x for x in self.components if isinstance(x, GeneralizedCoordinate)})
    
    @property
    def markers(self):
        return set({ x for x in self.components if isinstance(x, Marker)})
    
    @property
    def muscles(self):
        return set({ x for x in self.components if isinstance(x, Actuator)})
    
    def body(self, name: str):
        l = [x for x in self.bodies if x.name == name]
        if len(l) == 0:
            raise ValueError(f"No body found with name {name}")
        elif len(l) >= 2:
            raise ValueError(f"Two or more bodies found with name {name}")
        else:
            return l[0]
        
    def joint(self, name: str):
        l = [x for x in self.joints if x.name == name]
        if len(l) == 0:
            raise ValueError(f"No joint found with name {name}")
        elif len(l) >= 2:
            raise ValueError(f"Two or more joints found with name {name}")
        else:
            return l[0]

    def coordinate(self, name: str):
        l = [x for x in self.coordinates if x.name == name]
        if len(l) == 0:
            raise ValueError(f"No coordinate found with name {name}")
        elif len(l) >= 2:
            raise ValueError(f"Two or more coordinates found with name {name}")
        else:
            return l[0]
        
    def marker(self, name: str):
        l = [x for x in self.markers if x.name == name]
        if len(l) == 0:
            raise ValueError(f"No marker found with name {name}")
        elif len(l) >= 2:
            raise ValueError(f"Two or more markers found with name {name}")
        else:
            return l[0]
        
    def muscle(self, name: str):
        l = [x for x in self.muscles if x.name == name]
        if len(l) == 0:
            raise ValueError(f"No marker found with name {name}")
        elif len(l) >= 2:
            raise ValueError(f"Two or more markers found with name {name}")
        else:
            return l[0]
        
    def kinematic_jacobian(self, Q: dict, X: list, parent: Frame, coordinates: list[str | GeneralizedCoordinate], express_in: Frame = None):
        """Return the (Jl, Jw) the linear jacobian and the angular expressed in whatever frame you want,"""
        if express_in is None:
            express_in = self.ground

        coords = []
        for c in coordinates:
            if type(c) is str: coords.append(self.coordinate(c))
            else: coords.append(c)
        
        is_any_symbolic = False
        J = []
        for c in coords:
            joint = c.joint
            motion = c.motion

            w = joint.express_angular_velocity_in_parent(Q, motion)

            if isinstance(motion.axis, RotationAxis):
                X_at_end_of_joint = parent.express_in(Q, X, joint.child)
                X_ = X_at_end_of_joint.as_vector()
                X_in_joint = joint.child.express_in(Q, X_, joint.parent).as_point()
            else:
                X_in_joint = parent.express_in(Q, Point(X), joint)
            L = Line.from_direction_location(w, [0,0,0])
            T = Motor.translation(*X_in_joint.to_list()).inv()
            L_viewed_from_X = T.apply(L)
            dir_, moment_ = L_viewed_from_X.direction(), L_viewed_from_X.moment()

            dir_in_F = joint.parent.express_in(Q, dir_, express_in)
            moment_in_F = joint.parent.express_in(Q, moment_, express_in)
            
            if dir_in_F.is_symbolic() or moment_in_F.is_symbolic():
                is_any_symbolic = True

            if isinstance(motion.axis, RotationAxis):
                J.append(dir_in_F.to_list() + moment_in_F.to_list())
            elif isinstance(motion.axis, TranslationAxis):
                J.append([0,0,0] + dir_in_F.to_list())
        
        if is_any_symbolic:
            J = se.Matrix(J).T
        else: J = np.array(J).T

        J_angular, J_linear = Matrix(J[:3,:]), Matrix(J[3:,:])
        return J_linear, J_angular
    
    def lever_arm_matrix(self, Q: dict, coordinates: list[str | GeneralizedCoordinate], muscles: list[str | Actuator]):
        """Return L the lever arm matrix. The moment arm matrix is N = -L.T"""
        coords = []
        for c in coordinates:
            if type(c) is str: coords.append(self.coordinate(c))
            else: coords.append(c)

        musc = []
        for m in muscles:
            if type(m) is str: musc.append(self.muscle(m))
            else: musc.append(m)

        is_any_symbolic = False
        L = []
        for m in musc:
            lever_arm_vector = m.lever_arm(Q, coords)
            
            if Symbolic.list_is_symbolic(lever_arm_vector):
                is_any_symbolic = True
            L.append(lever_arm_vector)

        if is_any_symbolic:
            L = Matrix(se.Matrix(L)).T
        else: L = Matrix(np.array(L)).T

        return L

    def finalize(self):
        if not nx.is_directed_acyclic_graph(self.graph):
            raise Exception("The kinematic chain is not well-defined! It is not a directed acyclic graph. Check the joint links.")
        if not nx.is_tree(self.graph):
            raise Exception("The model is not well-defined! It is not a tree: check if all joints link properly every objects. Aborting finalization.")
        if self.ground not in self.components:
            raise Exception("The kinematic chain is not linked to the ground! Use model.ground to insert it.")
        for x in self.coordinates:
            if len(self.joints_of_coordinate(x)) > 1:
                raise Exception(f"The coordinate {x.name} is linked to multiple joints! This is not supported yet. Please use coupler constraints if a coordinate must act on multiple joints.")
            if len(self.motions_of_coordinate(x)) > 1:
                raise Exception(f"The coordinate {x.name} is linked to multiple motions! This is not supported yet. Please use coupler constraints if a coordinate must act on multiple joints.")
    
    def draw_graph(self):
        labels = {}
        for node in self.graph.nodes():
            try: 
                name = node.name
            except:
                if type(node) is Motion:
                    name = ""
                else: name = str(type(node))
            labels[node] = name

        nx.draw(self.graph, with_labels = True, labels=labels, node_size=100, arrowsize=15)