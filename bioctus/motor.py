import numpy as np
import symengine as se
from .hash import Hashable

def cos(x, is_symbolic: bool = False):
    if is_symbolic: 
        return se.cos(x)
    return np.cos(x)
    
def sin(x, is_symbolic: bool = False):
    if is_symbolic: 
        return se.sin(x)
    return np.sin(x)

def pi(is_symbolic: bool = False):
    if is_symbolic:
        return se.pi
    return np.pi

class Symbolic:
    def __init__(self):
        pass

    def to_list(self):
        pass

    def to_matrix(self):
        return se.Matrix(self.to_list())
    
    def to_numpy(self):
        """Returns the current object as a float numpy array, only if it is non-symbolic!
        Otherwise, raise an exception."""
        try:
            return np.array(self.to_list()).astype(float)
        except:
            raise ValueError("This object can't be cast as a numpy array! It contains symbolic elements in it.")
        
    def is_symbolic(self) -> bool:
        """Check if symbolic elements are present."""
        try:
            A = self.to_numpy()
            return False # if the downcast is not possible then it is necessarily symbolic.
        except:
            return True
    
    @staticmethod
    def list_is_symbolic(l) -> bool:
        """Check if symbolic elements are present in list."""
        try:
            A = np.array(l).astype(float)
            return False # if the downcast is not possible then it is necessarily symbolic.
        except:
            return True
        
    @staticmethod
    def element_is_symbolic(l) -> bool:
        """Check if symbolic elements is present."""
        try:
            A = np.array(l).astype(float)
            return False # if the downcast is not possible then it is necessarily symbolic.
        except:
            return True
    
    @staticmethod
    def convert_to_symbolic(l: any):
        """Convert an element or a list of elements to symbolic IF REQUIRED, so if there are strings in it for example."""
        if not isinstance(l, tuple | list | np.ndarray | se.Matrix):
            l = [l]

        L = []
        for x in l:
            if type(x) is str:
                L.append(se.symbols(x))
            else: L.append(x)
        
        if len(L) == 1:
            return L[0]
        return L
    
    def subs(self, *args, **kwarg):
        """Utility method to substitute symbolic values by specific values.
        Two possible way to use this function:

        1. Either use a dictionnary, for example:
            element.subs({ "x": 1, "y": 2 })
        2. Or substitute by keys:
            element.subs(x=1, y=2)

        You cannot use both ways at the same time.
        This function does nothing if no argument is given.
        """
        if self.is_symbolic() is False:
            return self
        
        dictionnary = {}
        if len(args) != 0:
            if isinstance(args[0], dict):
                dictionnary = args[0]
        for key, val in kwarg.items():
            dictionnary[str(key)] = val

        return self.to_matrix().subs(dictionnary)
    
class Matrix(Hashable, Symbolic):
    __slots__ = "matrix"
    def __init__(self, matrix):
        Symbolic.__init__(self)
        Hashable.__init__(self)

        self.matrix = matrix

    def __neg__(self):
        return Matrix(-self.matrix)
    
    @property
    def shape(self):
        return self.matrix.shape
    
    @property
    def T(self):
        return Matrix(self.matrix.T)
    
    def to_list(self):
        return self.matrix.tolist()
    
    def __str__(self):
        return self.__repr__()
    
    def __repr__(self):
        if self.is_symbolic():
            return str(se.Matrix(self.matrix))
        else:
            return str(np.array(self.matrix))
    
    def subs(self, *args, **kwarg):
        M = Symbolic.subs(self, *args, **kwarg)
        return Matrix(M)
    
    def lambdify(self, symbols: list, cse=True):
        return se.Lambdify(symbols, self.matrix, cse=cse)
    
class Point(Symbolic, Hashable):
    __slots__ = "x", "y", "z"
    def __init__(self, *args):
        Symbolic.__init__(self)
        Hashable.__init__(self)
    
        X = None
        if len(args) == 3:
            X = args
        elif len(args) == 1 and isinstance(args[0], Point | Vector):
            X = args[0].to_list()
        elif len(args) == 1 and isinstance(args[0], np.ndarray | list | se.Matrix):
            X = args[0]
            if len(X) != 3:
                raise ValueError("A point is defined by 3 elements.")
        else:
            raise ValueError("A point is defined by 3 elements.")
        
        self.x, self.y, self.z = Symbolic.convert_to_symbolic(X)

    def as_vector(self):
        return Vector(self.x, self.y, self.z)
    
    def to_list(self):
        return [self.x, self.y, self.z]
    
    def __sub__(self, other: "Point"):
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

    def __repr__(self):
        return f"Point(x={self.x}, y={self.y}, z={self.z})"
    
    def subs(self, *args, **kwarg):
        M = Symbolic.subs(self, *args, **kwarg)
        return Point(M)
    
class Vector(Symbolic, Hashable):
    __slots__ = "x", "y", "z"
    def __init__(self, *args):
        Symbolic.__init__(self)
        Hashable.__init__(self)
    
        X = None
        if len(args) == 3:
            X = args
        elif len(args) == 1 and isinstance(args[0], Point | Vector):
            X = args[0].to_list()
        elif len(args) == 1 and isinstance(args[0], np.ndarray | list | se.Matrix):
            X = args[0]
            if len(X) != 3:
                raise ValueError("A vector is defined by 3 elements.")
        else:
            raise ValueError("A vector is defined by 3 elements.")
        
        self.x, self.y, self.z = Symbolic.convert_to_symbolic(X)
            
    def as_point(self):
        return Point(self.x, self.y, self.z)
    
    def to_list(self):
        return [self.x, self.y, self.z]

    def norm(self):
        if self.is_symbolic(): 
            sqrt = se.sqrt
            x, y, z = self.to_list()
        else: 
            sqrt = np.sqrt
            x, y, z = self.to_numpy()

        return sqrt(x**2 + y**2 + z**2)
    
    def normal(self):
        return self / self.norm()
    
    def __add__(self, other: "Vector"):
        return Vector(self.x + other.x, self.y + other.y, self.z + other.z)
    
    def __sub__(self, other: "Vector"):
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

    def __mul__(self, other: any):
        return Vector(self.x * other, self.y * other, self.z * other)
    
    def __rmul__(self, other: any):
        return Vector(other * self.x, other * self.y, other * self.z)
    
    def __truediv__(self, other):
        return Vector(self.x / other, self.y / other, self.z / other)
    
    def cross(self, other):
        a1, a2, a3 = self.to_list()
        b1, b2, b3 = other.to_list()
        s1 = a2*b3 - a3*b2
        s2 = a3*b1 - a1*b3
        s3 = a1*b2 - a2*b1
        return Vector(s1, s2, s3)
    
    def dot(self, other):
        a1, a2, a3 = self.to_list()
        b1, b2, b3 = other.to_list()
        return a1*b1 + a2*b2 + a3*b3
    
    def __repr__(self):
        return f"Vector(x={self.x}, y={self.y}, z={self.z})"
    
    def subs(self, *args, **kwarg):
        M = Symbolic.subs(self, *args, **kwarg)
        return Vector(M)
    
class Line(Symbolic, Hashable):
    __slots__ = "a", "b", "c", "d", "e", "f"
    def __init__(self, *args):
        Symbolic.__init__(self)
        Hashable.__init__(self)

        X = None
        if len(args) == 6:
            X = args
        elif len(args) == 1 and isinstance(args[0], Line):
            X = args[0].to_list()
        elif len(args) == 1 and isinstance(args[0], np.ndarray | list | se.Matrix):
            X = args[0]
            if len(X) != 6:
                raise ValueError("A line is defined by 6 elements.")
        else:
            raise ValueError("A line is defined by 6 elements.")
        
        self.a, self.b, self.c, self.d, self.e, self.f = Symbolic.convert_to_symbolic(X)
            
    def to_list(self):
        return [self.a, self.b, self.c, self.d, self.e, self.f]
    
    @classmethod
    def from_points(cls, p1: tuple | list | np.ndarray | se.Matrix | Point, p2: tuple | list | np.ndarray | se.Matrix | Point):
        if isinstance(p1, Point):
            p1 = p1.to_list()
        elif isinstance(p1, tuple | list | np.ndarray | se.Matrix):
            if len(p1) != 3:
                raise ValueError("A point should be defined by 3 values!")
        else: 
            raise TypeError(f"Cannot convert type {type(p1)} as a point.")

        if isinstance(p2, Point):
            p2 = p2.to_list()
        elif isinstance(p2, tuple | list | np.ndarray | se.Matrix):
            if len(p2) != 3:
                raise ValueError("A point should be defined by 3 values!")
        else: 
            raise TypeError(f"Cannot convert type {type(p2)} as a point.")
                
        x1, y1, z1 = p1
        x2, y2, z2 = p2

        a = x2 - x1
        b = y2 - y1
        c = z2 - z1
        d = x1*y2 - x2*y1
        e = x1*z2 - x2*z1
        f = y1*z2 - y2*z1
        return Line(a, b, c, d, e, f)

    @classmethod
    def from_direction_location(cls, direction: tuple | list | np.ndarray | se.Matrix | Vector, location: tuple | list | np.ndarray | se.Matrix | Point):
        if isinstance(direction, Vector):
            direction = direction.to_list()
        elif isinstance(direction, tuple | list | np.ndarray | se.Matrix):
            if len(direction) != 3:
                raise ValueError("A point should be defined by 3 values!")
        else: 
            raise TypeError(f"Cannot convert type {type(direction)} as a point.")

        if isinstance(location, Point):
            location = location.to_list()
        elif isinstance(location, tuple | list | np.ndarray | se.Matrix):
            if len(location) != 3:
                raise ValueError("A point should be defined by 3 values!")
        else: 
            raise TypeError(f"Cannot convert type {type(location)} as a point.")
        x1, y1, z1 = location
        x2, y2, z2 = direction

        a = x2
        b = y2
        c = z2
        d = x1*y2 - x2*y1
        e = x1*z2 - x2*z1
        f = y1*z2 - y2*z1
        return Line(a, b, c, d, e, f)

    def direction(self):
        return Vector(self.a, self.b, self.c)
    
    def moment(self):
        return Vector(self.f, -self.e, self.d)
    
    def plucker_coordinates(self) -> list:
        """Returns a line as (lx, ly, lz, mx, my, mz)"""
        X = [self.a, self.b, self.c, self.f, -self.e, self.d]
        return X
    
    def lever_arm(self) -> Vector:
        """The lever arm vector"""
        return self.moment() / self.direction().norm()
    
    @staticmethod
    def from_plucker_coordinates(lx, ly, lz, mx, my, mz):
        return Line(lx, ly, lz, mz, -my, mx)
    
    def __repr__(self):
        a, b, c, d, e, f = self.to_list()
        return f"Line(a={a}, b={b}, c={c}, d={d}, e={e}, f={f})"
    
    def subs(self, *args, **kwarg):
        M = Symbolic.subs(self, *args, **kwarg)
        return Line(M)
    
class Motor(Symbolic, Hashable):
    __slots__ = "a", "b", "c", "d", "e", "f", "g", "h"
    def __init__(self, *args):
        Symbolic.__init__(self)
        Hashable.__init__(self)

        X = None
        if len(args) == 8:
            X = args
        elif len(args) == 1 and isinstance(args[0], Line):
            X = args[0].to_list()
        elif len(args) == 1 and isinstance(args[0], np.ndarray | list | se.Matrix):
            X = args[0]
            if len(X) != 8:
                raise ValueError("A motor is defined by 8 elements.")
        else:
            raise ValueError("A motor is defined by 8 elements.")
        
        self.a, self.b, self.c, self.d, self.e, self.f, self.g, self.h = Symbolic.convert_to_symbolic(X)
                
    def to_list(self):
        return [self.a, self.b, self.c, self.d, self.e, self.f, self.g, self.h]

    @classmethod
    def from_(cls, theta, axis_x, axis_y, axis_z, tx, ty, tz):
        theta, axis_x, axis_y, axis_z, tx, ty, tz = Symbolic.convert_to_symbolic([theta, axis_x, axis_y, axis_z, tx, ty, tz])
        is_symbolic = isinstance(theta, se.Expr)
        u, v = cos(theta/2, is_symbolic), sin(theta/2, is_symbolic)
        a, b, c = axis_x, axis_y, axis_z # EXTREMELY IMPORTANT TO REPLACE BY NEGATIVE FOR THE SECOND ONE
        e, f, g = tx/2, ty/2, tz/2

        C0 = [u] # scalar part
        C1 = [-c*v, b*v, -a*v] # e1^e2, e1^e3, e2^e3
        C2 = [b*g*v -c*f*v -e*u, -a*g*v + c*e*v - f*u, a*f*v - b*e*v - g*u] # Ptx = e1^ni, Pty = e2^ni, Ptz = e3^ni
        C3 = [v*(a*e + b*f + c*g)] # e1^e2^e3^ni

        return Motor(*C0, *C1, *C2, *C3)
    
    @classmethod
    def rotation(cls, theta, axis_x, axis_y, axis_z):
        """Axis-angle representation"""
        return Motor.from_(theta, axis_x, axis_y, axis_z, 0, 0, 0)
    
    @classmethod
    def rotation_euler(cls, seq: str, angles: list):
        """Initialize of rotation from Euler angles.

        The sequence and angles should be of length 2 or 3.
        The intrinsic sequence 'xyz' corresponds to a rotation around the X-axis, then the Y-axis, then the Z-axis.
        The extrinsic sequence 'XYZ' is equivalent to a rotation around the Z-axis, the the Y-axis, the the X-axis.
        """
        if len(seq) > 3:
            raise ValueError("Maximum 3 axes can be defined!")
        
        if not (seq.islower() or seq.isupper()):
            raise ValueError("Only lowercase sequence or uppercase sequence can be defined.")
        
        angles = Symbolic.convert_to_symbolic(angles)

        rot_list = []
        for k, el in enumerate(seq):
            angle = angles[k]
            if el == "x" or el == "X":
                rot_list = rot_list + [cls.rotation(angle, 1,0,0)]
            elif el == "y" or el == "Y":
                rot_list = rot_list + [cls.rotation(angle, 0,1,0)]
            elif el == "z" or el == "Z":
                rot_list = rot_list + [cls.rotation(angle, 0,0,1)]
            else:
                raise ValueError("The sequence contains characters different from 'x', 'y', 'z' or 'X', 'Y', 'Z'.")
        
        if seq.islower(): rot_list.reverse() # i.e. intrinsic/extrinsic rotations
        R = cls.identity()
        for x in rot_list:
            R = R*x
        return R
    
    @classmethod
    def translation(cls, tx, ty, tz):
        return Motor.from_(0, 0, 0, 0, tx, ty, tz)
    
    def inv(self):
        a, b, c, d, e, f, g, h = self.to_list()
        return Motor(a, -b, -c, -d, -e, -f, -g, h)

    def __mul__(self, other):
        a1, b1, c1, d1, e1, f1, g1, h1 = self.to_list()
        a2, b2, c2, d2, e2, f2, g2, h2 = other.to_list()

        a = a1*a2 - b1*b2 - c1*c2 - d1*d2
        b = a1*b2 + a2*b1 - c1*d2 + c2*d1   # e1^e2
        c = a1*c2 + a2*c1 + b1*d2 - b2*d1  # e1^e3
        d = a1*d2 + a2*d1 - b1*c2 + b2*c1   # e2^e3
        e = a1*e2 + a2*e1 + b1*f2 - b2*f1 + c1*g2 - c2*g1 - d1*h2 - d2*h1 # e1^ni
        f = a1*f2 + a2*f1 - b1*e2 + b2*e1 + c1*h2 + c2*h1 + d1*g2 - d2*g1 # e2^ni
        g = a1*g2 + a2*g1 - b1*h2 - b2*h1 - c1*e2 + c2*e1 - d1*f2 + d2*f1 # e3^ni
        h = a1*h2 + a2*h1 + b1*g2 + b2*g1 - c1*f2 - c2*f1 + d1*e2 + d2*e1 # e1^e2^e3^ni

        return  Motor(a,b,c,d,e,f,g,h)
    
    def apply(self, x):
        if isinstance(x, Point): return self.apply_on_point(x)
        elif isinstance(x, Vector): return self.apply_on_vector(x)
        elif isinstance(x, Line): return self.apply_on_line(x)
        else: raise TypeError("The object is not a point, vector or line.")

    def apply_on_point(self, p: Point):
        a,b,c,d,e,f,g,h = self.to_list()
        x, y, z = p.to_list()
        
        xp = a**2*x + 2*a*b*y + 2*a*c*z - 2*a*e - b**2*x + 2*b*d*z - 2*b*f - c**2*x - 2*c*d*y - 2*c*g + d**2*x - 2*d*h
        yp = a**2*y - 2*a*b*x + 2*a*d*z - 2*a*f - b**2*y - 2*b*c*z + 2*b*e + c**2*y - 2*c*d*x + 2*c*h - d**2*y - 2*d*g
        zp = a**2*z - 2*a*c*x - 2*a*d*y - 2*a*g + b**2*z - 2*b*c*y + 2*b*d*x - 2*b*h - c**2*z + 2*c*e - d**2*z + 2*d*f
        return Point(xp, yp, zp)
    
    def apply_on_vector(self, v: Vector):
        a,b,c,d,e,f,g,h = self.to_list()
        x, y, z = v.to_list()
        xp = a**2*x + 2*a*b*y + 2*a*c*z - b**2*x + 2*b*d*z - c**2*x - 2*c*d*y + d**2*x
        yp = a**2*y - 2*a*b*x + 2*a*d*z - b**2*y - 2*b*c*z + c**2*y - 2*c*d*x - d**2*y
        zp = a**2*z - 2*a*c*x - 2*a*d*y + b**2*z - 2*b*c*y + 2*b*d*x - c**2*z - d**2*z
        return Vector(xp, yp, zp)
    
    def apply_on_line(self, l: Line):   
        a,b,c,d,e,f,g,h = self.to_list()
        l1, l2, l3, m1, m2, m3 = l.to_list()

        l1_ = a**2*l1 + 2*a*b*l2 + 2*a*c*l3 - b**2*l1 + 2*b*d*l3 - c**2*l1 - 2*c*d*l2 + d**2*l1 # no^e1^ni
        l2_ = a**2*l2 - 2*a*b*l1 + 2*a*d*l3 - b**2*l2 - 2*b*c*l3 + c**2*l2 - 2*c*d*l1 - d**2*l2 # no^e2^ni
        l3_ = a**2*l3 - 2*a*c*l1 - 2*a*d*l2 + b**2*l3 - 2*b*c*l2 + 2*b*d*l1 - c**2*l3 - d**2*l3 # no^e3^ni
        m1_ = a**2*m1 - 2*a*c*m3 + 2*a*d*m2 - 2*a*e*l2 + 2*a*f*l1 - 2*a*h*l3 + b**2*m1 + 2*b*c*m2 + 2*b*d*m3 + 2*b*e*l1 + 2*b*f*l2 + 2*b*g*l3 - c**2*m1 + 2*c*f*l3 - 2*c*g*l2 + 2*c*h*l1 - d**2*m1 - 2*d*e*l3 + 2*d*g*l1 + 2*d*h*l2 # e1^e2^ni
        m2_ = a**2*m2 + 2*a*b*m3 - 2*a*d*m1 - 2*a*e*l3 + 2*a*g*l1 + 2*a*h*l2 - b**2*m2 + 2*b*c*m1 - 2*b*f*l3 + 2*b*g*l2 - 2*b*h*l1 + c**2*m2 + 2*c*d*m3 + 2*c*e*l1 + 2*c*f*l2 + 2*c*g*l3 - d**2*m2 + 2*d*e*l2 - 2*d*f*l1 + 2*d*h*l3 # e1^e3^ni
        m3_ = a**2*m3 - 2*a*b*m2 + 2*a*c*m1 - 2*a*f*l3 + 2*a*g*l2 - 2*a*h*l1 - b**2*m3 + 2*b*d*m1 + 2*b*e*l3 - 2*b*g*l1 - 2*b*h*l2 - c**2*m3 + 2*c*d*m2 - 2*c*e*l2 + 2*c*f*l1 - 2*c*h*l3 + d**2*m3 + 2*d*e*l1 + 2*d*f*l2 + 2*d*g*l3 # e2^e3^ni

        return Line(l1_, l2_, l3_, m1_, m2_, m3_)
        
    @classmethod
    def identity(cls):
        return Motor(1, 0, 0, 0, 0, 0, 0, 0)

def translation(tx, ty, tz):
    return Motor.translation(tx, ty, tz)

def rotation(theta, x, y, z):
    return Motor.rotation(theta, x, y, z)

def rotation_euler(seq: str, angles: list = None):
    return Motor.rotation_euler(seq, angles)