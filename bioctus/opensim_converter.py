from .functions import Function, Identity, Constant, Multiplier, Linear, SimmSpline, PiecewiseConstant, PiecewiseLinear, Polynomial
from .model import Model
from .motor import Point, Vector, Line
from .components import PathPoint, HillThelenMuscle, Muscle, Actuator, Ground, Marker, Body, Frame, OffsetFrame, GeneralizedCoordinate, Joint, Motion, RotationAxis, TranslationAxis, WeldJoint, PinJoint, CustomAlignedJoint, PlanarJoint, UniversalJoint, SliderJoint, BallJoint, EulerJoint, GimbalJoint, FreeJoint

def opensim_to_bioctus(path: str):
    import opensim as osim
    
    def find_by_osim_object(l: list, val):
        for x in l:
            if x["osim"].isEqualTo(val):
                return x
        raise ValueError("No object found.")

    def create_bioctus_function(fct_osim):
        import opensim as osim
        class_name = fct_osim.getConcreteClassName()
        name = fct_osim.getName()
        if class_name == "Constant":
            fct_osim: osim.Constant = osim.Constant.safeDownCast(fct_osim)
            fct_bioctus = Constant(name, fct_osim.getValue())
        elif class_name == "LinearFunction":
            fct_osim: osim.LinearFunction = osim.LinearFunction.safeDownCast(fct_osim)
            slope, intercept = fct_osim.getSlope(), fct_osim.getIntercept()
            if slope == 1 and intercept == 0:
                return None
            else:
                fct_bioctus = Linear(name, slope, intercept)
        elif class_name == "PiecewiseConstantFunction":
            fct_osim: osim.PiecewiseConstantFunction = osim.PiecewiseConstantFunction.safeDownCast(fct_osim)
            X, Y = [], []
            for k in range(fct_osim.getNumberOfPoints()):
                X.append(fct_osim.getX(k))
                Y.append(fct_osim.getY(k))
            fct_bioctus = PiecewiseConstant(name, X, Y)
        elif class_name == "PiecewiseLinearFunction":
            fct_osim: osim.PiecewiseLinearFunction = osim.PiecewiseLinearFunction.safeDownCast(fct_osim)
            X, Y = [], []
            for k in range(fct_osim.getNumberOfPoints()):
                X.append(fct_osim.getX(k))
                Y.append(fct_osim.getY(k))
            fct_bioctus = PiecewiseLinear(name, X, Y)
        elif class_name == "SimmSpline":
            fct_osim: osim.SimmSpline = osim.SimmSpline.safeDownCast(fct_osim)
            X, Y = [], []
            for k in range(fct_osim.getNumberOfPoints()):
                X.append(fct_osim.getX(k))
                Y.append(fct_osim.getY(k))
            fct_bioctus = SimmSpline(name, X, Y)
        elif class_name == "MultplierFunction":
            fct_osim: osim.MultplierFunction = osim.MultplierFunction.safeDownCast(fct_osim)
            sub_fct = fct_osim.getFunction()
            fct_bioctus = Multiplier(name, create_bioctus_function(sub_fct))
        elif class_name == "PolynomialFunction":
            fct_osim: osim.PolynomialFunction = osim.PolynomialFunction.safeDownCast(fct_osim)
            coefficients = fct_osim.getCoefficients().to_numpy()
            fct_bioctus = Polynomial(name, coefficients)
        else:
            raise TypeError("OpenSim function type", class_name, " is not available in Bioctus.")

        return fct_bioctus

    model_osim = osim.Model(path)
    state = model_osim.initSystem()

    model_bioctus = Model(model_osim.getName())

    # 1.1 Ground, bodies, frames and offset frames
    frames = []
    for f in model_osim.getFrameList():
        if f.getConcreteClassName() == "Ground":
            frames.append({ "osim": f, "bioctus": model_bioctus.ground })

        elif f.getConcreteClassName() == "Frame":
            frame = Frame(name=f.getName())
            frames.append({"bioctus": frame, "osim": f})

        elif f.getConcreteClassName() == "PhysicalOffsetFrame":
            f: osim.PhysicalOffsetFrame = osim.PhysicalOffsetFrame.safeDownCast(f)
            frame = OffsetFrame(name=f.getName(), orientation=f.get_orientation().to_numpy().tolist(), translation=f.get_translation().to_numpy().tolist())
            frames.append({"bioctus": frame, "osim": f})

        elif f.getConcreteClassName() == "Body":
            f: osim.Body = osim.Body.safeDownCast(f)
            body = Body(
                name=f.getName(), 
                mass=f.getMass(), 
                mass_center=f.getMassCenter().to_numpy().tolist(), 
                inertia=f.getInertia().getMoments().to_numpy().tolist()
                + f.getInertia().getProducts().to_numpy().tolist()
            )
            frames.append({ "osim": f, "bioctus": body })

        else: raise TypeError(f"Impossible to convert OpenSim {f.getConcreteClassName()} object!")

    # 1.2 Ensure linking offset frames properly
    for f in model_osim.getFrameList():
        if f.getConcreteClassName() == "PhysicalOffsetFrame":
            f: osim.PhysicalOffsetFrame = osim.PhysicalOffsetFrame.safeDownCast(f)
            find_by_osim_object(frames, f)["bioctus"].parent = find_by_osim_object(frames, f.getParentFrame())["bioctus"]

    # 2.1 Coordinates
    coordinates = []
    for c in model_osim.getCoordinateSet():
        c: osim.Coordinate = osim.Coordinate.safeDownCast(c)
        coordinate = GeneralizedCoordinate(
            name=c.getName(),
            default_value=c.getDefaultValue(),
            default_speed_value=c.getDefaultSpeedValue(),
            range_values=[c.getRangeMin(), c.getRangeMax()],
            clamped=c.getDefaultClamped(),
            locked=c.getDefaultLocked(),
        )
        coordinates.append({ "osim": c, "bioctus": coordinate})

    # 2.2 Coupling coordinates
    for c in model_osim.getConstraintSet():
        if c.getConcreteClassName() == "CoordinateCouplerConstraint":
            c: osim.CoordinateCouplerConstraint = osim.CoordinateCouplerConstraint.safeDownCast(c)
            dependent_coord = find_by_osim_object(coordinates, model_osim.getCoordinateSet().get(c.getDependentCoordinateName()))
            indepent_coord_names = c.getIndependentCoordinateNames()
            if indepent_coord_names.getSize() >= 2:
                raise Exception("Bioctus do not handle coordinates depending on more than one other coordinate.")
            for idx_indep in range(indepent_coord_names.getSize()):
                indep_coord_name = indepent_coord_names.get(idx_indep)
                indep_coord = find_by_osim_object(coordinates, model_osim.getCoordinateSet().get(indep_coord_name))
                dependent_coord["bioctus"].independent_coordinate = indep_coord["bioctus"]
                dependent_coord["bioctus"].coupling_constraint = create_bioctus_function(c.getFunction())

            if c.get_isEnforced():
                dependent_coord["bioctus"].enforce_constraint = True
            else:
                dependent_coord["bioctus"].enforce_constraint = False

    # 3. Joints
    joints = []
    for x in model_osim.getJointSet():
        x: osim.Joint = osim.Joint.safeDownCast(x)

        parent = find_by_osim_object(frames, x.getParentFrame())["bioctus"]
        child = find_by_osim_object(frames, x.getChildFrame())["bioctus"]

        coords_joint = [x.get_coordinates(i) for i in range(x.numCoordinates())]

        j = None
        joint_type = x.getConcreteClassName()
        if joint_type == "WeldJoint":
            j = WeldJoint(x.getName(), parent, child)
        elif joint_type == "PinJoint":
            j = PinJoint(x.getName(), find_by_osim_object(coordinates, coords_joint[0])["bioctus"], parent, child)
        elif joint_type == "SliderJoint":
            j = SliderJoint(x.getName(), find_by_osim_object(coordinates, coords_joint[0])["bioctus"], parent, child)
        elif joint_type == "PlanarJoint":
            j = PlanarJoint(x.getName(), [find_by_osim_object(coordinates, c)["bioctus"] for c in coords_joint], parent, child)
        elif joint_type == "UniversalJoint":
            j = UniversalJoint(x.getName(), [find_by_osim_object(coordinates, c)["bioctus"] for c in coords_joint], parent, child)
        elif joint_type == "GimbalJoint":
            j = GimbalJoint(x.getName(), [find_by_osim_object(coordinates, c)["bioctus"] for c in coords_joint], parent, child)
        elif joint_type == "BallJoint":
            j = BallJoint(x.getName(), [find_by_osim_object(coordinates, c)["bioctus"] for c in coords_joint], parent, child)
        elif joint_type == "FreeJoint":
            j = FreeJoint(x.getName(), [find_by_osim_object(coordinates, c)["bioctus"] for c in coords_joint], parent, child)
        elif joint_type == "CustomJoint":
            ### BE VERY CAREFUL! Rotations are XYZ according to opensim, then translations applied
            x: osim.CustomJoint = osim.CustomJoint.safeDownCast(x)
            
            st: osim.SpatialTransform = x.getSpatialTransform()        
            r1: osim.TransformAxis = st.get_rotation1()
            r2: osim.TransformAxis = st.get_rotation2()
            r3: osim.TransformAxis = st.get_rotation3()
            t1: osim.TransformAxis = st.get_translation1()
            t2: osim.TransformAxis = st.get_translation2()
            t3: osim.TransformAxis = st.get_translation3()
            
            rotations = []
            translations = []
            l = 0
            
            for ta in [r1, r2, r3, t1, t2, t3]: 
                coordinates_axis = ta.getCoordinateNamesInArray()
                if coordinates_axis.getSize() > 1:
                    raise ValueError("A transform axis with multiple coordinates is not available in Bioctus! Aborting.")
                
                if ta in [r1, r2, r3]: # ROTATIONS
                    if coordinates_axis.getSize() == 1:
                        coord_ta = model_osim.getCoordinateSet().get(ta.get_coordinates(0))
                        coordinate_bioctus = find_by_osim_object(coordinates, coord_ta)["bioctus"]
                        
                        angle_function = None
                        if r1.getFunction() is not None:
                            angle_function = create_bioctus_function(ta.getFunction())

                        m = Motion(
                            coordinate=coordinate_bioctus,
                            axis=RotationAxis(*ta.get_axis().to_numpy().tolist()),
                            function=angle_function
                        )
                        rotations.append(m)
                        l+=1
                else: # TRANSLATIONS
                    if coordinates_axis.getSize() == 1:
                        coord_ta = model_osim.getCoordinateSet().get(ta.get_coordinates(0))
                        coordinate_bioctus = find_by_osim_object(coordinates, coord_ta)["bioctus"]

                        angle_function = None
                        if r1.getFunction() is not None:
                            angle_function = create_bioctus_function(ta.getFunction())

                        m = Motion(
                            coordinate=coordinate_bioctus,
                            axis=TranslationAxis(*ta.get_axis().to_numpy().tolist()),
                            function=angle_function
                        )
                        translations.append(m)
                        l+=1

            rotations.reverse()
            motions = rotations + translations
            j = Joint(
                name=x.getName(),
                motions=motions,
                parent=parent,
                child=child
            )
        else:
            raise TypeError(f"Cannot convert yet joint of type {joint_type}!")
            
        joints.append({ "bioctus": j, "osim": x })
    
    for j in joints:
        model_bioctus.link(j["bioctus"])


    # Markers
    markers = []
    for m in model_osim.getMarkerSet():
        m: osim.Marker = m
        marker = Marker(
            name=m.getName(),
            location=m.get_location().to_numpy().tolist(),
            parent=find_by_osim_object(frames, m.getParentFrame())["bioctus"],
            marker_type="anatomical" if m.get_fixed() else "technical"
        )
        markers.append({"bioctus": marker, "osim": m})

    for m in markers:
        model_bioctus.add(m["bioctus"])

    # Muscles
    muscles = []
    path_points = []
    for m in model_osim.getMuscles():
        m: osim.Muscle = osim.Muscle.safeDownCast(m)

        geometry_path: osim.GeometryPath = m.getGeometryPath()
        path_point_set = geometry_path.getPathPointSet()

        path_points_ = []
        for k in range(path_point_set.getSize()):
            p = path_point_set.get(k)

            if p.getConcreteClassName() == "PathPoint":
                p: osim.PathPoint = osim.PathPoint.safeDownCast(p)
                location = p.get_location().to_numpy().tolist()
            elif p.getConcreteClassName() == "MovingPathPoint":
                p: osim.MovingPathPoint = osim.MovingPathPoint.safeDownCast(p)
                # THIS IS A TEMPORARY SITUATION
                location = [
                    p.get_x_location().calcValue(osim.Vector([0])),
                    p.get_y_location().calcValue(osim.Vector([0])),
                    p.get_z_location().calcValue(osim.Vector([0])),
                ]

            parent_frame_osim = p.getParentFrame()
            parent_frame_ = find_by_osim_object(frames, parent_frame_osim)

            pp = PathPoint(p.getName(), location, parent_frame_["bioctus"])
            path_points_.append(pp)
            path_points.append({ "bioctus": pp, "osim": p})

        muscle = HillThelenMuscle(
            name=m.getName(),
            path_points=path_points_,
            max_isometric_force=m.getMaxIsometricForce(),
            optimal_fiber_length=m.getOptimalFiberLength(),
            tendon_slack_length=m.getTendonSlackLength(),
            pennation_angle_at_optimal_fiber_length=m.getPennationAngleAtOptimalFiberLength()
        )

        muscles.append({ "bioctus": muscle, "osim": m})
    
    for m in path_points:
        model_bioctus.add(m["bioctus"])
    for m in muscles:
        model_bioctus.add(m["bioctus"])

    model_bioctus.finalize()
    
    return model_bioctus