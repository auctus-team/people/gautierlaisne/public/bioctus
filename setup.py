from setuptools import setup, find_packages

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='bioctus',
   version='0.1.0',
   description='Symbolic musculoskeletal model using motor algebra.',
   license="MIT",
   long_description=long_description,
   author='Gautier LAISNE',
   author_email='gautier.laisne@inria.fr',
   url="https://gitlab.inria.fr/auctus-team/people/gautierlaisne/public/bioctus/",
   packages=find_packages(),
   install_requires=['networkx', 'numpy', 'symengine']
)